package memory

import (
	"catalog/genproto/catalog_service"
	"catalog/packages/helper"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type categoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) *categoryRepo {

	return &categoryRepo{
		db: db,
	}

}

func (c *categoryRepo) Create(ctx context.Context, req *catalog_service.CreateCategory) (string, error) {

	var id = uuid.NewString()

	query := `
	INSERT INTO
	categories (id,parent_id,title,image,order_number)
	VALUES ($1,$2,$3,$4,$5)`

	_, err := c.db.Exec(ctx, query,
		id,
		req.ParentId,
		req.Title,
		req.Image,
		req.OrderNumber,
	)

	fmt.Println(req.ParentId)
	fmt.Println(req.Image)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil
}

func (c *categoryRepo) Update(ctx context.Context, req *catalog_service.Category) (string, error) {

	query := `
	UPDATE
	categories
	SET
		parent_id=$2,title=$3,image=$4,active=$5,order_number=$6,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
		req.ParentId,
		req.Title,
		req.Image,
		req.Active,
		req.OrderNumber,
	)
	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}
	return "OK", nil
}

func (c *categoryRepo) Get(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.Category, error) {

	query := `
	SELECT
	id,
	parent_id,
	title,
	image,
	active,
	order_number,
	created_at::text,
	updated_at::text
	FROM categories
	WHERE id=$1`

	resp := c.db.QueryRow(ctx, query, req.Id)

	var category catalog_service.Category

	err := resp.Scan(
		&category.Id,
		&category.ParentId,
		&category.Title,
		&category.Image,
		&category.Active,
		&category.OrderNumber,
		&category.CreatedAt,
		&category.UpdatedAt,
	)

	if err != nil {
		return &catalog_service.Category{}, err
	}

	return &category, nil
}

func (c *categoryRepo) Delete(ctx context.Context, req *catalog_service.IdReqRes) (string, error) {

	query := `UPDATE categories SET deleted_at=NOW() WHERE id = $1`

	resp, err := c.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (c *categoryRepo) GetAll(ctx context.Context, req *catalog_service.GetAllCategoryRequest) (resp *catalog_service.GetAllCategoryResponse, err error) {

	var (
		params  = make(map[string]interface{})
		filter  = " WHERE deleted_at IS NULL AND true "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `
	SELECT
	id,
	parent_id,
	title,
	image,
	active,
	order_number,
	created_at::text,
	updated_at::text
	FROM categories
	`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		categories
	`

	if req.Title != "" {
		filter += ` AND title ILIKE '%' || @title || '%' `
		params["title"] = req.Title
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	q, pArr := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, q, pArr...)
	if err != nil {
		return &catalog_service.GetAllCategoryResponse{}, err
	}

	err = c.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &catalog_service.GetAllCategoryResponse{}, err
	}

	defer rows.Close()

	result := []*catalog_service.Category{}

	for rows.Next() {

		var category catalog_service.Category

		err := rows.Scan(
			&category.Id,
			&category.ParentId,
			&category.Title,
			&category.Image,
			&category.Active,
			&category.OrderNumber,
			&category.CreatedAt,
			&category.UpdatedAt,
		)
		if err != nil {
			return &catalog_service.GetAllCategoryResponse{}, err
		}

		result = append(result, &category)

	}

	return &catalog_service.GetAllCategoryResponse{Categories: result, Count: int64(count)}, nil

}
