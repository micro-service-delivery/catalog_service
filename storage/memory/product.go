package memory

import (
	"catalog/genproto/catalog_service"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
)

type productRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *productRepo {

	return &productRepo{
		db: db,
	}

}

func (p *productRepo) Create(ctx context.Context, req *catalog_service.CreateProduct) (string, error) {

	id := uuid.NewString()

	query := `
	INSERT INTO
		products(id,title,description,photos,order_number,type,price,category_id)
	VALUES($1,$2,$3,$4,$5,$6,$7,$8)`

	_, err := p.db.Exec(ctx, query,
		id,
		req.Title,
		req.Description,
		req.Photos,
		req.OrderNumber,
		req.Type,
		req.Price,
		req.CategoryId,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return "", err
	}

	return id, nil

}

func (p *productRepo) Update(ctx context.Context, req *catalog_service.Product) (string, error) {

	query := `
	UPDATE
		products
	SET
		title=$2,description=$3,photos=$4,order_number=$5,type=$6,price=$7,category_id=$8,active=$9,updated_at=NOW()
	WHERE
		id=$1`

	resp, err := p.db.Exec(ctx, query,
		req.Id,
		req.Title,
		req.Description,
		req.Photos,
		req.OrderNumber,
		req.Type,
		req.Price,
		req.CategoryId,
		req.Active,
	)
	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}
	return "OK", nil
}

func (p *productRepo) Get(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.Product, error) {

	query := `SELECT
	id,
	title,
	description,
	photos,
	order_number,
	type,
	price,
	category_id,
	active,
	created_at::text,
	updated_at::text
	FROM products WHERE id=$1`

	resp := p.db.QueryRow(ctx, query, req.Id)

	var product catalog_service.Product

	err := resp.Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.Photos,
		&product.OrderNumber,
		&product.Type,
		&product.Price,
		&product.CategoryId,
		&product.Active,
		&product.CreatedAt,
		&product.UpdatedAt,
	)

	if err != nil {
		fmt.Println("Error from Select")
		return &catalog_service.Product{}, err
	}

	return &product, nil
}

func (t *productRepo) Delete(ctx context.Context, req *catalog_service.IdReqRes) (string, error) {

	query := `UPDATE products SET deleted_at=NOW() WHERE id = $1`

	resp, err := t.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return "", err
	}
	if resp.RowsAffected() == 0 {
		return "", pgx.ErrNoRows
	}

	return "Deleted suc", nil
}

func (t *productRepo) GetAll(ctx context.Context, req *catalog_service.GetAllProductRequest) (resp *catalog_service.GetAllProductResponse, err error) {

	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0 "
		limit   = " LIMIT 10 "
		offset  = (req.GetPage() - 1) * req.GetLimit()
	)

	info := `SELECT
	id,
	title,
	description,
	photos,
	order_number,
	type,
	price,
	category_id,
	active,
	created_at::text,
	updated_at::text
	FROM products`

	var count int

	cQ := `
	SELECT
		COUNT(*)
	FROM 
		products
	`

	if req.Title != "" {
		filter += ` AND title ILIKE ` + "'%" + req.GetTitle() + "%'"
	}

	if req.Type != "" {
		filter += ` AND type ILIKE ` + "'%" + req.GetType() + "%'"
	}

	if req.CategoryId != "" {
		filter += ` AND category_id='` + req.GetCategoryId() + `' `
	}

	if req.Active {
		filter += ` AND active=true `
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf(" OFFSET %d", offset)
	}

	query := info + filter + limit + offsetQ
	countQuery := cQ + filter

	rows, err := t.db.Query(ctx, query)
	if err != nil {
		return &catalog_service.GetAllProductResponse{}, err
	}

	err = t.db.QueryRow(context.Background(), countQuery).Scan(&count)

	if err != nil {
		return &catalog_service.GetAllProductResponse{}, err
	}

	defer rows.Close()

	result := []*catalog_service.Product{}

	for rows.Next() {

		var product catalog_service.Product

		err := rows.Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.Photos,
			&product.OrderNumber,
			&product.Type,
			&product.Price,
			&product.CategoryId,
			&product.Active,
			&product.CreatedAt,
			&product.UpdatedAt,
		)
		if err != nil {
			return &catalog_service.GetAllProductResponse{}, err
		}

		result = append(result, &product)

	}

	return &catalog_service.GetAllProductResponse{Products: result, Count: int64(count)}, nil
}
