package grpc_client

import (
	"catalog/config"
	"catalog/genproto/order_service"
	"catalog/genproto/person_service"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// GrpcClientI ...
type GrpcClientI interface {
	UserService() person_service.UserServiceClient
	OrderService() order_service.OrderServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (*GrpcClient, error) {

	connOrder, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.OrderServiceHost, cfg.OrderServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("order service dial host: %s port:%d err: %s",
			cfg.OrderServiceHost, cfg.OrderServisePort, err)
	}

	connPerson, err := grpc.Dial(fmt.Sprintf("%s:%d", cfg.PersonServiceHost, cfg.PersonServisePort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("person service dial host: %s port:%d err: %s",
			cfg.PersonServiceHost, cfg.PersonServisePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service":  person_service.NewUserServiceClient(connPerson),
			"order_service": order_service.NewOrderServiceClient(connOrder),
			// "tarif_service":              staff_service.NewTarifServerClient(connStaff),
			// "sale_service":               sale_service.NewSaleServerClient(connSale),
			// "sale_product_service":       sale_service.NewSaleProductServerClient(connSale),
			// "branch_transaction_service": sale_service.NewBranchTransactionServerClient(connSale),
			// "transaction_service":        sale_service.NewTransactionServerClient(connSale),
		},
	}, nil
}

func (g *GrpcClient) UserService() person_service.UserServiceClient {
	return g.connections["user_service"].(person_service.UserServiceClient)
}

func (g *GrpcClient) OrderService() order_service.OrderServiceClient {
	return g.connections["order_service"].(order_service.OrderServiceClient)
}

// func (g *GrpcClient) StaffService() staff_service.StaffServerClient {
// 	return g.connections["staff_service"].(staff_service.StaffServerClient)
// }

// func (g *GrpcClient) SaleService() sale_service.SaleServerClient {
// 	return g.connections["sale_service"].(sale_service.SaleServerClient)
// }

// func (g *GrpcClient) SaleProductService() sale_service.SaleProductServerClient {
// 	return g.connections["sale_product_service"].(sale_service.SaleProductServerClient)
// }

// func (g *GrpcClient) BranchTransactionService() sale_service.BranchTransactionServerClient {
// 	return g.connections["branch_transaction_service"].(sale_service.BranchTransactionServerClient)
// }

// func (g *GrpcClient) TransactionService() sale_service.TransactionServerClient {
// 	return g.connections["transaction_service"].(sale_service.TransactionServerClient)
// }
