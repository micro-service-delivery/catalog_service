package service

import (
	"catalog/genproto/catalog_service"
	grpc_client "catalog/grpc/client"
	"catalog/packages/logger"
	"catalog/storage"
	"context"
)

type ProductService struct {
	logger  logger.LoggerI
	storage storage.StoregeI
	clients grpc_client.GrpcClientI
	catalog_service.UnimplementedProductServiceServer
}

func NewProductService(log logger.LoggerI, strg storage.StoregeI, grpcClients grpc_client.GrpcClientI) *ProductService {
	return &ProductService{
		logger:  log,
		storage: strg,
		clients: grpcClients,
	}
}

func (t *ProductService) Create(ctx context.Context, req *catalog_service.CreateProduct) (*catalog_service.IdReqRes, error) {
	id, err := t.storage.Product().Create(ctx, req)
	if err != nil {
		return nil, err
	}

	return &catalog_service.IdReqRes{Id: id}, nil
}

func (t *ProductService) Update(ctx context.Context, req *catalog_service.Product) (*catalog_service.ResponseString, error) {
	str, err := t.storage.Product().Update(ctx, req)
	if err != nil {
		return nil, err
	}

	return &catalog_service.ResponseString{Text: str}, nil
}

func (t *ProductService) Get(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.Product, error) {
	product, err := t.storage.Product().Get(ctx, req)
	if err != nil {
		return nil, err
	}

	return product, nil
}

func (t *ProductService) GetAll(ctx context.Context, req *catalog_service.GetAllProductRequest) (*catalog_service.GetAllProductResponse, error) {
	products, err := t.storage.Product().GetAll(ctx, req)
	if err != nil {
		return nil, err
	}

	return products, nil
}

func (t *ProductService) Delete(ctx context.Context, req *catalog_service.IdReqRes) (*catalog_service.ResponseString, error) {
	text, err := t.storage.Product().Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &catalog_service.ResponseString{Text: text}, nil
}
