CREATE TYPE "type_product" AS ENUM (
  'modifier',
  'product'
);

CREATE TABLE "categories" (
  "id" uuid PRIMARY KEY,
  "title" varchar NOT NULL,
  "image" varchar,
  "active" bool DEFAULT true,
  "parent_id" uuid,
  "order_number" integer,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp 
);

CREATE TABLE "products" (
  "id" uuid PRIMARY KEY,
  "title" varchar NOT NULL,
  "description" text,
  "photos" varchar,
  "order_number" integer,
  "active" bool DEFAULT true,
  "type" type_product,
  "price" numeric NOT NULL,
  "category_id" uuid,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp 
);

ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
